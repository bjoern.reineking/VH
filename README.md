
VIGRA ( "Vision with Generic Algorithms") Headers for R

### About

This package provides [R](https://www.r-project.org) with access to
[VIGRA](https://ukoethe.github.io/vigra/) header files. A large part of
[VIGRA](https://ukoethe.github.io/vigra/) is provided as C++ template code which is
resolved entirely at compile-time without linking.  

This package aims to provide the [VIGRA](https://ukoethe.github.io/vigra/)
libraries for easy integration in R via Rcpp. 

It can be used via the `LinkingTo:` field in the `DESCRIPTION` field of an R
package --- and the R package infrastructure tools will then know how to set
include flags correctly on all architectures supported by R.

Note that this can be used solely by the headers-only part of the VIGRA  libraries. This covers most of VIGRA, but excludes some libraries which require linking for parts or all of their functionality (e.g. image import/export). However, no effort was made to extract for this package only that part of VIGRA that can b

### Installation

You can install the released version of VH from [gitlab](https://gitlab.irstea.fr) with:

``` r
library(devtools)
devtools::install_git('https://gitlab.irstea.fr/bjoern.reineking/VH.git')
```


### Coverage

The complete include folder of vigra-1.11.1 is included in this package.

### See Also

This package is strongly inspired by the  [BH](http://dirk.eddelbuettel.com/code/bh.html) package providing BOOST headers for R (and this README contains verbatim copies of the BH README.md)

## License

This package is provided under the same license as VIGRA itself, the MIT.
