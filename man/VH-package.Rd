\name{VH-package}
\alias{VH-package}
\alias{VH}
\docType{package}
\title{\packageTitle{VH}}
\description{\packageDescription{VH}}
\details{The DESCRIPTION file: \packageDESCRIPTION{VH}}
\section{Package Content}{\packageIndices{VH}}
\author{\packageAuthor{VH}}
\section{Maintainer}{\packageMaintainer{VH}}
\references{https://ukoethe.github.io/vigra/}
\keyword{ package }
